// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

#![no_std]
#![no_main]

/**
 * This example checks the PWM channels and the output pins for changing the direction.
 * After 2 seconds, the direction of the motors change (CW -> CCW -> CW -> ...).
 * This is also indicated with the led blue which toggles.
 */
// The macro for our start-up function
use seeeduino_xiao_rp2040::entry;

// GPIO traits
use embedded_hal::digital::OutputPin;
use embedded_hal::pwm::SetDutyCycle;

use embedded_hal::delay::DelayNs;

// Ensure we halt the program on panic (if we don't mention this crate it won't
// be linked)
use panic_halt as _;

use xiao_rp2040::board;

/// Entry point to our bare-metal application.
///
/// The `#[entry]` macro ensures the Cortex-M start-up code calls this function
/// as soon as all global variables are initialised.
#[entry]
fn main() -> ! {
    let board = board::XiaoRp2040::create();
    let mut board = board::Board::from(board);

    // Power off leds red and green
    board.red_led.set_high().unwrap();
    board.green_led.set_high().unwrap();

    // Set duty cycle
    let _ = board.pwm_x.set_duty_cycle_percent(10_u8);
    let _ = board.pwm_y.set_duty_cycle_percent(20_u8);
    let _ = board.pwm_z.set_duty_cycle_percent(30_u8);

    loop {
        board.blue_led.set_low().unwrap();
        board.direction_pin_x.set_low().unwrap();
        board.direction_pin_y.set_low().unwrap();
        board.direction_pin_z.set_low().unwrap();
        board.timer.delay_ms(2000);

        board.blue_led.set_high().unwrap();
        board.direction_pin_x.set_high().unwrap();
        board.direction_pin_y.set_high().unwrap();
        board.direction_pin_z.set_high().unwrap();
        board.timer.delay_ms(2000);
    }
}
