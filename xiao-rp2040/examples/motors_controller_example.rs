// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

#![no_std]
#![no_main]

/**
 * This example shows how to use the MotorController.
 */
// The macro for our start-up function
use seeeduino_xiao_rp2040::entry;

use embedded_hal::{delay::DelayNs, digital::OutputPin};

// Ensure we halt the program on panic (if we don't mention this crate it won't
// be linked)
use panic_halt as _;

use cube::actuators::{MotorDirection, MotorsController};
use cube::hal::Devices;
use xiao_rp2040::board;

/// Entry point to our bare-metal application.
///
/// The `#[entry]` macro ensures the Cortex-M start-up code calls this function
/// as soon as all global variables are initialised.
#[entry]
fn main() -> ! {
    let board = board::XiaoRp2040::create();
    let board = board::Board::from(board);
    let (mut utils, mut motors_controller, _serial_port, _imu) = board.get_devices();

    // Power off red and green leds
    utils.red_led.set_high().unwrap();
    utils.green_led.set_high().unwrap();

    loop {
        motors_controller.set_direction([
            MotorDirection::Clockwise,
            MotorDirection::Clockwise,
            MotorDirection::Clockwise,
        ]);
        utils.blue_led.set_low().unwrap(); // Blue Led on

        motors_controller.set_pwm([25_u8, 25_u8, 25_u8]);
        utils.timer.delay_ms(1000);
        motors_controller.set_pwm([100_u8, 100_u8, 100_u8]);
        utils.timer.delay_ms(1000);
        motors_controller.set_pwm([25_u8, 25_u8, 25_u8]);
        utils.timer.delay_ms(1000);

        motors_controller.set_direction([
            MotorDirection::CounterClockwise,
            MotorDirection::CounterClockwise,
            MotorDirection::CounterClockwise,
        ]);
        utils.blue_led.set_high().unwrap(); // Blue led off

        utils.timer.delay_ms(1000);
        motors_controller.set_pwm([100_u8, 100_u8, 100_u8]);
        utils.timer.delay_ms(1000);
        motors_controller.set_pwm([25_u8, 25_u8, 25_u8]);
        utils.timer.delay_ms(1000);
    }
}
