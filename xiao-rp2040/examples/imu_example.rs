// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

#![no_std]
#![no_main]

/**
 * This example shows how to use the IMU.
 */
// The macro for our start-up function
use seeeduino_xiao_rp2040::entry;

use embedded_hal::{delay::DelayNs, digital::OutputPin};
use embedded_hal_nb::serial::Write;

// Ensure we halt the program on panic (if we don't mention this crate it won't
// be linked)
use panic_halt as _;

use arrayvec::ArrayString;
use core::fmt::Write as FmtWrite;
use cube::hal::Devices;
use mpu6050_dmp::{quaternion::Quaternion, yaw_pitch_roll::YawPitchRoll};
use xiao_rp2040::board;

/// Entry point to our bare-metal application.
///
/// The `#[entry]` macro ensures the Cortex-M start-up code calls this function
/// as soon as all global variables are initialised.
#[entry]
fn main() -> ! {
    let board = board::XiaoRp2040::create();
    let board = board::Board::from(board);
    let (mut utils, mut _motors_controller, mut serial_port, mut imu) = board.get_devices();

    // Power off red and green leds
    utils.red_led.set_high().unwrap();
    utils.green_led.set_high().unwrap();

    let message = "Starting\n\r";
    for byte in message.bytes() {
        let result = serial_port.write(byte);
        if let Err(_) = result {
            utils.red_led.set_low().unwrap();
        }
    }

    let message = "Initializing IMU\n\r";
    for byte in message.bytes() {
        serial_port.write(byte).unwrap();
    }
    imu.initialize_dmp(&mut utils.timer).unwrap();
    let message = "IMU initialization completed\n\r";
    for byte in message.bytes() {
        serial_port.write(byte).unwrap();
    }

    let mut flag: bool = true;
    loop {
        // TODO is there a better way to toggle a led?
        if flag {
            utils.blue_led.set_low().unwrap(); // Blue Led on
            flag = false;
        } else {
            utils.blue_led.set_high().unwrap(); // Blue led off
            flag = true;
        }

        let len = imu.get_fifo_count().unwrap();
        if len >= 28 {
            let mut buf = [0; 28];
            let buf = imu.read_fifo(&mut buf).unwrap();
            let q = Quaternion::from_bytes(&buf[..16]).unwrap();
            let ypr = YawPitchRoll::from(q);

            let mut buf = ArrayString::<100>::new();
            writeln!(
                &mut buf,
                "Yaw {:.1} Pitch {:.1} Roll {:.1}\n\r",
                ypr.yaw, ypr.pitch, ypr.roll
            )
            .unwrap();
            for byte in buf.bytes() {
                serial_port.write(byte).unwrap();
            }
        }

        utils.timer.delay_ms(1000);
    }
}
