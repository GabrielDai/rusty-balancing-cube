// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use ::core::convert::From;
use cube;

// Pull in any important traits
//use seeeduino_xiao_rp2040::hal::prelude::*;

// A shorter alias for the Peripheral Access Crate, which provides low-level
// register access
use seeeduino_xiao_rp2040::hal::pac;
// A shorter alias for the Hardware Abstraction Layer, which provides
// higher-level drivers.
use hal::gpio::PinState;
use hal::Clock;
use seeeduino_xiao_rp2040::hal;

use fugit::RateExtU32;

pub struct XiaoRp2040 {
    pub pac_i2c: pac::I2C1,
    pub pac_uart: pac::UART0,
    pub pac_resets: pac::RESETS,
    pub clocks: hal::clocks::ClocksManager,
    pub pins: seeeduino_xiao_rp2040::Pins,
    pub timer: hal::Timer,
    pub pwm_slices: hal::pwm::Slices,
}

impl XiaoRp2040 {
    pub fn create() -> Self {
        let mut pac = pac::Peripherals::take().unwrap();
        // let core = pac::CorePeripherals::take().unwrap();
        let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);
        let clocks = hal::clocks::init_clocks_and_plls(
            seeeduino_xiao_rp2040::XOSC_CRYSTAL_FREQ,
            pac.XOSC,
            pac.CLOCKS,
            pac.PLL_SYS,
            pac.PLL_USB,
            &mut pac.RESETS,
            &mut watchdog,
        )
        .ok()
        .unwrap();

        let sio = hal::Sio::new(pac.SIO); // The single-cycle I/O block controls our GPIO pins
        let pins = seeeduino_xiao_rp2040::Pins::new(
            pac.IO_BANK0,
            pac.PADS_BANK0,
            sio.gpio_bank0,
            &mut pac.RESETS,
        );
        let pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);
        let timer = hal::Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

        XiaoRp2040 {
            pac_i2c: pac.I2C1,
            pac_uart: pac.UART0,
            pac_resets: pac.RESETS,
            clocks: clocks,
            pins: pins,
            timer: timer,
            pwm_slices: pwm_slices,
        }
    }

    pub fn config(&mut self) {
        self.config_pwm();
    }

    fn config_pwm(&mut self) {
        // 20kHz desired frequency
        // Rp2040 clock = 125MHz
        // Top = 1_023 (65_535, maximum possible u16 value)
        // Wrap = Top+1 (number of possible values)
        // Phase correction multiplier = 2 if phase correction enabled, else 1

        // Divider = rp2040_clock / (Wrap * phase_correction_multiplier * desired_frequency)
        // Divider = 125,000,000/(1_024 * 2 * 20000)
        // Divider = 3.051757
        // Divider int = 3
        // Divider frac = 1 (1/16 = 0.0625)
        // PWM Motor 1 & 2: Configure PWM5

        const TOP: u16 = 1_023_u16;

        let pwm5 = &mut self.pwm_slices.pwm5;
        pwm5.set_ph_correct();
        pwm5.set_top(TOP);
        pwm5.set_div_int(3);
        pwm5.set_div_frac(1);
        pwm5.enable();

        let pwm2 = &mut self.pwm_slices.pwm2;
        pwm2.set_ph_correct();
        pwm2.set_top(TOP);
        pwm2.set_div_int(3);
        pwm2.set_div_frac(1);
        pwm2.enable();
    }
}

use hal::gpio::bank0;
use hal::gpio::FunctionI2c;
use hal::gpio::FunctionSio;
use hal::gpio::FunctionUart;
use hal::gpio::PullDown;
use hal::gpio::PullUp;
use hal::gpio::SioOutput;
use hal::pwm;
use hal::uart::{DataBits, StopBits, UartConfig};

pub type Board = cube::hal::BoardPeripherals<
    pwm::Channel<hal::pwm::Slice<hal::pwm::Pwm5, hal::pwm::FreeRunning>, hal::pwm::A>,
    pwm::Channel<hal::pwm::Slice<hal::pwm::Pwm5, hal::pwm::FreeRunning>, hal::pwm::B>,
    pwm::Channel<hal::pwm::Slice<hal::pwm::Pwm2, hal::pwm::FreeRunning>, hal::pwm::A>,
    hal::gpio::Pin<bank0::Gpio28, FunctionSio<SioOutput>, PullDown>,
    hal::gpio::Pin<bank0::Gpio29, FunctionSio<SioOutput>, PullDown>,
    hal::gpio::Pin<bank0::Gpio3, FunctionSio<SioOutput>, PullDown>,
    hal::gpio::Pin<bank0::Gpio2, FunctionSio<SioOutput>, PullDown>,
    hal::gpio::Pin<bank0::Gpio17, FunctionSio<SioOutput>, PullDown>,
    hal::gpio::Pin<bank0::Gpio16, FunctionSio<SioOutput>, PullDown>,
    hal::gpio::Pin<bank0::Gpio25, FunctionSio<SioOutput>, PullDown>,
    hal::I2C<
        pac::I2C1,
        (
            hal::gpio::Pin<bank0::Gpio6, FunctionI2c, PullUp>,
            hal::gpio::Pin<bank0::Gpio7, FunctionI2c, PullUp>,
        ),
    >,
    hal::uart::UartPeripheral<
        hal::uart::Enabled,
        pac::UART0,
        (
            hal::gpio::Pin<bank0::Gpio0, FunctionUart, PullDown>,
            hal::gpio::Pin<bank0::Gpio1, FunctionUart, PullDown>,
        ),
    >,
    hal::timer::Timer,
>;

impl From<XiaoRp2040> for Board {
    fn from(mut board: XiaoRp2040) -> Self {
        board.config();

        // PWM modules
        let mut pwm5_channel_a = board.pwm_slices.pwm5.channel_a;
        let mut pwm5_channel_b = board.pwm_slices.pwm5.channel_b;
        let mut pwm2_channel_a = board.pwm_slices.pwm2.channel_a;
        pwm5_channel_a.output_to(board.pins.a0); // Gpio26
        pwm5_channel_b.output_to(board.pins.a1); // Gpio27
        pwm2_channel_a.output_to(board.pins.miso); // Gpio4

        // Pins
        let output_1 = board.pins.a2.into_push_pull_output_in_state(PinState::Low);
        let output_2 = board.pins.a3.into_push_pull_output_in_state(PinState::Low);
        let output_3 = board
            .pins
            .mosi
            .into_push_pull_output_in_state(PinState::Low);
        let output_4 = board.pins.sck.into_push_pull_output_in_state(PinState::Low);
        let led_red = board
            .pins
            .led_red
            .into_push_pull_output_in_state(PinState::Low);
        let led_green = board
            .pins
            .led_green
            .into_push_pull_output_in_state(PinState::Low);
        let led_blue = board
            .pins
            .led_blue
            .into_push_pull_output_in_state(PinState::Low);

        // I2C
        let sda_pin: hal::gpio::Pin<_, hal::gpio::FunctionI2C, _> = board.pins.sda.reconfigure();
        let scl_pin: hal::gpio::Pin<_, hal::gpio::FunctionI2C, _> = board.pins.scl.reconfigure();
        let i2c = hal::I2C::i2c1(
            board.pac_i2c,
            sda_pin,
            scl_pin,
            400.kHz(),
            &mut board.pac_resets,
            &board.clocks.system_clock,
        );

        // UART
        let uart_pins = (board.pins.tx.into_function(), board.pins.rx.into_function());
        let uart = hal::uart::UartPeripheral::new(board.pac_uart, uart_pins, &mut board.pac_resets)
            .enable(
                UartConfig::new(115200.Hz(), DataBits::Eight, None, StopBits::One),
                board.clocks.peripheral_clock.freq(),
            )
            .unwrap();

        Board {
            pwm_x: pwm5_channel_a,
            pwm_y: pwm5_channel_b,
            pwm_z: pwm2_channel_a,
            direction_pin_x: output_1,
            direction_pin_y: output_2,
            direction_pin_z: output_3,
            motors_enable: output_4,
            red_led: led_red,
            green_led: led_green,
            blue_led: led_blue,
            i2c: i2c,
            uart: uart,
            timer: board.timer,
        }
    }
}
