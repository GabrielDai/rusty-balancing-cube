// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use crate::actuators;
use crate::control;
use crate::sensors;
use crate::setpoint;
use crate::setpoint::BalancingPoint;
use libm;

// Balance controller core logic
pub struct Core<M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    control: [control::LinearQuadraticRegulator; 2],
    attitude_monitor: A,
    motors: [M; 3],
    setpoint_selector: B,
}

impl<M, A, B> Core<M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    pub fn create() -> Self {
        // TODO use constants
        Core {
            control: [
                control::LinearQuadraticRegulator::create([150f32, 14f32, 0.035f32]),
                control::LinearQuadraticRegulator::create([150f32, 14f32, 0.035f32]),
            ],
            attitude_monitor: A::create(10f32, 0.996f32, [0.6f32, 0.6f32]),
            motors: [M::create(), M::create(), M::create()],
            setpoint_selector: B::create(),
        }
    }

    pub fn init(&mut self) {
        let measurements: usize = 1024;
        self.attitude_monitor
            .measure_angle_velocity_offset(measurements);

        let limits = [
            [0.4f32, 0.4f32],
            [3f32, 0.6f32],
            [6f32, 0.6f32],
            [0.6f32, 3f32],
        ];
        self.setpoint_selector.set_balancing_point_limits(limits);

        self.reset_offsets();
    }

    pub fn config(&mut self) {
        // TODO routine for offsets reading
    }

    pub fn reset_offsets(&mut self) {
        self.setpoint_selector
            .set_offset(BalancingPoint::Vertex, [0f32, 0f32]);
        self.setpoint_selector
            .set_offset(BalancingPoint::EdgeX, [0f32, 0f32]);
        self.setpoint_selector
            .set_offset(BalancingPoint::EdgeY, [0f32, 0f32]);
        self.setpoint_selector
            .set_offset(BalancingPoint::EdgeZ, [0f32, 0f32]);
    }

    pub fn control_balance(&mut self) {
        let angle = self.attitude_monitor.get_current_angle();
        self.setpoint_selector.discover_balancing_point(angle);
        let offset = self.setpoint_selector.get_offset();
        let angle = [angle[0] - offset[0], angle[1] - offset[1]];

        let angular_velocity = self.attitude_monitor.get_filtered_angular_velocity();
        let mut motors_input = [0f32, 0f32];
        let control_input = [
            [angle[0], angular_velocity[2]],
            [angle[1], angular_velocity[1]],
        ];
        for i in 0..2 {
            let output = self.control[i].step(control_input[i]);
            motors_input[i] = output.min(255f32);
            motors_input[i] = motors_input[i].max(-255f32);
            self.control[i].sum_output(motors_input[i]);
        }

        self.control_motors(motors_input);
    }

    fn control_motors(&mut self, motors_input: [f32; 2]) {
        // TODO this logic is for one balancing point
        let mut duty = [
            libm::roundf(0.5f32 * motors_input[0] - 0.75f32 * motors_input[1]),
            libm::roundf(0.5f32 * motors_input[0] + 0.75f32 * motors_input[1]),
            motors_input[0],
        ];

        for d in duty.iter_mut() {
            *d = d.min(255f32);
            *d = d.max(-255f32);
        }

        for i in 0..3 {
            // TODO verify direction
            if duty[i] > 0f32 {
                self.motors[i].set_direction(actuators::MotorDirection::Clockwise);
            } else {
                self.motors[i].set_direction(actuators::MotorDirection::CounterClockwise);
            }
            self.motors[i].set_pwm(255u8 - libm::fabsf(duty[i]) as u8);
        }
    }

    pub fn standby(&mut self) {
        for i in 0..3 {
            self.motors[i].brake();
        }
        for i in 0..2 {
            self.control[i].reset();
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {}
}
