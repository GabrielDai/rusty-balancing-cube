// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use crate::actuators;
use embedded_hal::{delay::DelayNs, digital::OutputPin, i2c::I2c, pwm::SetDutyCycle};
use embedded_hal_nb::serial;

pub struct BoardUtils<RLED, GLED, BLED, T>
where
    RLED: OutputPin,
    GLED: OutputPin,
    BLED: OutputPin,
    T: DelayNs,
{
    pub red_led: RLED,
    pub green_led: GLED,
    pub blue_led: BLED,
    pub timer: T,
}

pub trait Devices {
    type Utils;
    type MotorsController;
    type SerialPort;
    type Imu;

    fn get_devices(
        self,
    ) -> (
        Self::Utils,
        Self::MotorsController,
        Self::SerialPort,
        Self::Imu,
    );
}

pub struct BoardPeripherals<PX, PY, PZ, DX, DY, DZ, E, RLED, GLED, BLED, I, U, T>
where
    PX: SetDutyCycle,
    PY: SetDutyCycle,
    PZ: SetDutyCycle,
    DX: OutputPin,
    DY: OutputPin,
    DZ: OutputPin,
    E: OutputPin,
    RLED: OutputPin,
    GLED: OutputPin,
    BLED: OutputPin,
    I: I2c,
    U: serial::Read + serial::Write + serial::ErrorType,
    T: DelayNs,
{
    pub pwm_x: PX,
    pub pwm_y: PY,
    pub pwm_z: PZ,
    pub direction_pin_x: DX,
    pub direction_pin_y: DY,
    pub direction_pin_z: DZ,
    pub motors_enable: E,
    pub red_led: RLED,
    pub green_led: GLED,
    pub blue_led: BLED,
    pub i2c: I,
    pub uart: U,
    pub timer: T,
}

impl<PX, PY, PZ, DX, DY, DZ, E, RLED, GLED, BLED, I, U, T> Devices
    for BoardPeripherals<PX, PY, PZ, DX, DY, DZ, E, RLED, GLED, BLED, I, U, T>
where
    PX: SetDutyCycle,
    PY: SetDutyCycle,
    PZ: SetDutyCycle,
    DX: OutputPin,
    DY: OutputPin,
    DZ: OutputPin,
    E: OutputPin,
    RLED: OutputPin,
    GLED: OutputPin,
    BLED: OutputPin,
    I: I2c,
    U: serial::Read + serial::Write + serial::ErrorType,
    T: DelayNs,
{
    type Utils = BoardUtils<RLED, GLED, BLED, T>;
    type MotorsController = actuators::GenericMotorsController<PX, PY, PZ, DX, DY, DZ, E>;
    type SerialPort = U;
    type Imu = mpu6050_dmp::sensor::Mpu6050<I>;

    fn get_devices(
        self,
    ) -> (
        Self::Utils,
        Self::MotorsController,
        Self::SerialPort,
        Self::Imu,
    ) {
        // TODO use imu to create AttitudeMonitor
        // TODO check i2c address used
        let imu = Self::Imu::new(self.i2c, mpu6050_dmp::address::Address::default()).unwrap();
        (
            Self::Utils {
                red_led: self.red_led,
                green_led: self.green_led,
                blue_led: self.blue_led,
                timer: self.timer,
            },
            Self::MotorsController::create(
                self.pwm_x,
                self.pwm_y,
                self.pwm_z,
                self.direction_pin_x,
                self.direction_pin_y,
                self.direction_pin_z,
                self.motors_enable,
            ),
            self.uart,
            imu,
        )
    }
}
