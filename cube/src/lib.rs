// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

#![no_std]
#![allow(dead_code)]

pub mod actuators;
mod balance_controller;
mod control;
pub mod hal;
mod sensors;
mod setpoint;

use ::core::convert::Into;
use balance_controller::BalanceController;
use balance_controller::StateMachine;

pub struct Cube<BF>
where
    BF: hal::Devices,
{
    board: BF,
}

impl<BF> Cube<BF>
where
    BF: hal::Devices,
{
    pub fn create(some_board: BF) -> Self {
        Cube { board: some_board }
    }

    pub fn execute_balance_controller<M, A, B>()
    where
        M: actuators::Motor,
        A: sensors::AttitudeMonitor,
        B: setpoint::BalancingPointSelector,
    {
        let mut controller: BalanceController<M, A, B> =
            BalanceController::Init(StateMachine::create());
        loop {
            controller = controller.execute_state();

            if false {
                // TODO Command: DetectingBalancingPoint -> Config
                if let BalanceController::DetectingBalancingPoint(val) = controller {
                    controller = BalanceController::Config(val.into());
                }
            }
        }
    }
}
