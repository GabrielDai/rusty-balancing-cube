// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use embedded_hal::{digital::OutputPin, pwm::SetDutyCycle};

pub enum MotorDirection {
    Clockwise,
    CounterClockwise,
}

// TODO deprecate this trait
pub trait Motor {
    fn create() -> Self;
    fn set_pwm(&mut self, duty: u8);
    fn set_direction(&mut self, direction: MotorDirection);
    fn brake(&mut self);
}

pub trait MotorsController {
    fn set_pwm(&mut self, duty: [u8; 3]);
    fn set_direction(&mut self, direction: [MotorDirection; 3]);
    fn brake(&mut self);
}

pub struct GenericMotorsController<PX, PY, PZ, DX, DY, DZ, E>
where
    PX: SetDutyCycle,
    PY: SetDutyCycle,
    PZ: SetDutyCycle,
    DX: OutputPin,
    DY: OutputPin,
    DZ: OutputPin,
    E: OutputPin,
{
    pwm_x: PX,
    pwm_y: PY,
    pwm_z: PZ,
    direction_pin_x: DX,
    direction_pin_y: DY,
    direction_pin_z: DZ,
    motors_enable: E,
}

impl<PX, PY, PZ, DX, DY, DZ, E> GenericMotorsController<PX, PY, PZ, DX, DY, DZ, E>
where
    PX: SetDutyCycle,
    PY: SetDutyCycle,
    PZ: SetDutyCycle,
    DX: OutputPin,
    DY: OutputPin,
    DZ: OutputPin,
    E: OutputPin,
{
    pub fn create(
        pwm_x: PX,
        pwm_y: PY,
        pwm_z: PZ,
        direction_pin_x: DX,
        direction_pin_y: DY,
        direction_pin_z: DZ,
        motors_enable: E,
    ) -> Self {
        GenericMotorsController {
            pwm_x,
            pwm_y,
            pwm_z,
            direction_pin_x,
            direction_pin_y,
            direction_pin_z,
            motors_enable,
        }
    }
}

impl<PX, PY, PZ, DX, DY, DZ, E> MotorsController
    for GenericMotorsController<PX, PY, PZ, DX, DY, DZ, E>
where
    PX: SetDutyCycle,
    PY: SetDutyCycle,
    PZ: SetDutyCycle,
    DX: OutputPin,
    DY: OutputPin,
    DZ: OutputPin,
    E: OutputPin,
{
    fn set_pwm(&mut self, duty: [u8; 3]) {
        let _ = self
            .pwm_x
            .set_duty_cycle_fraction(u16::from(duty[0]), 255_u16);
        let _ = self
            .pwm_y
            .set_duty_cycle_fraction(u16::from(duty[1]), 255_u16);
        let _ = self
            .pwm_z
            .set_duty_cycle_fraction(u16::from(duty[2]), 255_u16);

        self.motors_enable.set_high().unwrap();
    }

    fn set_direction(&mut self, direction: [MotorDirection; 3]) {
        match direction[0] {
            MotorDirection::Clockwise => self.direction_pin_x.set_low().unwrap(),
            MotorDirection::CounterClockwise => self.direction_pin_x.set_high().unwrap(),
        }
        match direction[1] {
            MotorDirection::Clockwise => self.direction_pin_y.set_low().unwrap(),
            MotorDirection::CounterClockwise => self.direction_pin_y.set_high().unwrap(),
        }
        match direction[2] {
            MotorDirection::Clockwise => self.direction_pin_z.set_low().unwrap(),
            MotorDirection::CounterClockwise => self.direction_pin_z.set_high().unwrap(),
        }
    }

    fn brake(&mut self) {
        self.motors_enable.set_low().unwrap();
    }
}
