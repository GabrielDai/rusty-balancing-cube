// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

//use stack_map;

//#[derive(Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Debug)]
pub enum BalancingPoint {
    NoBalacingPointRecognized,
    Vertex,
    EdgeX,
    EdgeY,
    EdgeZ,
}

//type OffsetMap = stack_map::StackMap::<BalancingPoint, [f32; 2], 5>;
//type BalancingPointLimitMap = stack_map::StackMap::<BalancingPoint, [f32; 2], 4>;

pub trait BalancingPointSelector {
    fn create() -> Self;
    fn set_balancing_point_limits(&mut self, limit: [[f32; 2]; 4]);
    fn discover_balancing_point(&mut self, angle: [f32; 2]);
    fn get_offset(&self) -> [f32; 2];
    fn get_balancing_point(&self) -> BalancingPoint;
    fn set_offset(&mut self, balancing_point: BalancingPoint, offset: [f32; 2]);
}

/*
pub struct Selector {
    balancing_point: BalancingPoint,
    offset: OffsetMap,
    balancing_point_limit: BalancingPointLimitMap,
}

impl BalancingPointSelector for Selector {
    fn create() -> Self {
        let mut default_offset = OffsetMap::default();
        default_offset.insert(BalancingPoint::NoBalacingPointRecognized, [0f32, 0f32]);
        default_offset.insert(BalancingPoint::Vertex, [0f32, 0f32]);
        default_offset.insert(BalancingPoint::EdgeX, [0f32, 0f32]);
        default_offset.insert(BalancingPoint::EdgeY, [0f32, 0f32]);
        default_offset.insert(BalancingPoint::EdgeZ, [0f32, 0f32]);

        let mut default_limit = BalancingPointLimitMap::default();
        default_limit.insert(BalancingPoint::Vertex, [0f32, 0f32]);
        default_limit.insert(BalancingPoint::EdgeX, [0f32, 0f32]);
        default_limit.insert(BalancingPoint::EdgeY, [0f32, 0f32]);
        default_limit.insert(BalancingPoint::EdgeZ, [0f32, 0f32]);

        Selector {
            balancing_point: BalancingPoint::NoBalacingPointRecognized,
            offset: default_offset,
            balancing_point_limit: default_limit,
        }
    }

    fn set_balancing_point_limits(&mut self, limit: [[f32;2]; 4]) {
        // Default value expected [[0.4f32, 0.4f32], [3f32, 0.6f32], [6f32, 0.6f32], [0.6f32, 3f32]]
        self.balancing_point_limit.insert(BalancingPoint::Vertex, limit[0]);
        self.balancing_point_limit.insert(BalancingPoint::EdgeX, limit[1]);
        self.balancing_point_limit.insert(BalancingPoint::EdgeY, limit[2]);
        self.balancing_point_limit.insert(BalancingPoint::EdgeZ, limit[3]);
    }

    fn discover_balancing_point(&mut self, angle: [f32; 2]) {
        // TODO check enum correlation with hardcoded values (Vertex is OK)
        let mut new_balancing_point = BalancingPoint::NoBalacingPointRecognized;

        for (point, limit) in self.balancing_point_limit.iter() {
            let offset = self.offset.get(&point).unwrap();
            if (angle[0] - offset[0]).abs() < limit[0] &&
               (angle[1] - offset[1]).abs() < limit[1] {
                new_balancing_point = *point;
                break;
            }
        }
        self.balancing_point = new_balancing_point;
    }

    fn get_offset(&self) -> [f32; 2] {
        *self.offset.get(&self.balancing_point).unwrap()
    }

    fn get_balancing_point(&self) -> BalancingPoint {
        self.balancing_point
    }

    fn set_offset(&mut self, balancing_point: BalancingPoint, offset: [f32; 2]) {
        self.offset.insert(balancing_point, offset);
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn init_state() {
        let mut selector = Selector::create();
        assert_eq!(selector.get_balancing_point(), BalancingPoint::NoBalacingPointRecognized);
        assert_eq!(selector.get_offset(), [0f32, 0f32]);

        selector.discover_balancing_point([0f32, 0f32]);
        assert_eq!(selector.get_balancing_point(), BalancingPoint::NoBalacingPointRecognized);
    }

    #[test]
    fn discover_balancing_point() {
        let mut selector = Selector::create();
        let limits = [[0.4f32, 0.4f32], [3f32, 0.6f32], [6f32, 0.6f32], [0.6f32, 3f32]];
        selector.set_balancing_point_limits(limits);

        selector.discover_balancing_point([-0.2f32, -0.1f32]);
        assert_eq!(selector.get_balancing_point(), BalancingPoint::Vertex);

        selector.discover_balancing_point([2f32, -0.1f32]);
        assert_eq!(selector.get_balancing_point(), BalancingPoint::EdgeX);

        selector.discover_balancing_point([-5f32, -0.5f32]);
        assert_eq!(selector.get_balancing_point(), BalancingPoint::EdgeY);

        selector.discover_balancing_point([0.1f32, -2f32]);
        assert_eq!(selector.get_balancing_point(), BalancingPoint::EdgeZ);
    }

    #[test]
    fn get_offset() {
        let mut selector = Selector::create();
        let limits = [[0.4f32, 0.4f32], [3f32, 0.6f32], [6f32, 0.6f32], [0.6f32, 3f32]];
        selector.set_balancing_point_limits(limits);

        selector.set_offset(BalancingPoint::NoBalacingPointRecognized, [1f32, 2f32]);
        selector.set_offset(BalancingPoint::Vertex, [3f32, 4f32]);
        selector.set_offset(BalancingPoint::EdgeX, [5f32, 6f32]);
        selector.set_offset(BalancingPoint::EdgeY, [7f32, 8f32]);
        selector.set_offset(BalancingPoint::EdgeZ, [9f32, 10f32]);

        assert_eq!(selector.get_offset(), [1f32, 2f32]);

        selector.discover_balancing_point([3.2f32, 4.1f32]);
        assert_eq!(selector.get_offset(), [3f32, 4f32]);

        selector.discover_balancing_point([2.1f32, 6.1f32]);
        assert_eq!(selector.get_offset(), [5f32, 6f32]);

        selector.discover_balancing_point([12.1f32, 8.5f32]);
        assert_eq!(selector.get_offset(), [7f32, 8f32]);

        selector.discover_balancing_point([9.2f32, 11.3f32]);
        assert_eq!(selector.get_offset(), [9f32, 10f32]);
    }
}
*/
