// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use crate::control;
use control::Filter;
use libm;

pub trait AttitudeMonitor {
    fn create(period: f32, fusion_factor: f32, filter_constant: [f32; 2]) -> Self;
    fn measure_angle_velocity_offset(&mut self, times: usize);
    fn get_current_angle(&mut self) -> [f32; 2];
    fn get_filtered_angular_velocity(&mut self) -> [f32; 3];
}

pub struct AttitudeMonitorI2c {
    // TODO interface i2c and IMU driver
    period: f32, // TODO use better type for time
    fusion_factor: f32,
    angular_velocity: [f32; 3],
    angular_velocity_offset: [f32; 3],
    cube_angle: [f32; 2],
    filter: [control::LowPassFilter; 2],
}

impl AttitudeMonitorI2c {
    fn fusion_measurements(&mut self, acceleration_angle: [f32; 2]) {
        for i in 0..2 {
            self.cube_angle[i] = self.cube_angle[i] * self.fusion_factor
                + acceleration_angle[i] * (1f32 - self.fusion_factor);
        }
    }
}

impl AttitudeMonitor for AttitudeMonitorI2c {
    fn create(period: f32, fusion_factor: f32, filter_constant: [f32; 2]) -> Self {
        AttitudeMonitorI2c {
            period: period,
            fusion_factor: fusion_factor,
            angular_velocity: [0f32, 0f32, 0f32],
            angular_velocity_offset: [0f32, 0f32, 0f32],
            cube_angle: [0f32, 0f32],
            filter: [
                control::LowPassFilter::create(filter_constant[0]),
                control::LowPassFilter::create(filter_constant[1]),
            ],
        }
    }

    fn measure_angle_velocity_offset(&mut self, times: usize) {
        // TODO check if this should be following a sequence
        let mut sum = [0f32, 0f32];
        for i in 0..times {
            self.get_current_angle();
            for j in 1..3 {
                sum[i] += self.angular_velocity[j];
                // self.angular_velocity[0] is not used
            }
            // TODO delay
        }

        for i in 0..2 {
            self.angular_velocity_offset[i] = sum[i] / times as f32;
        }
    }

    fn get_current_angle(&mut self) -> [f32; 2] {
        let dummy_angular_velocity = [1i16, 2i16, 3i16]; // TODO read from IMU
        let dummy_acceleration = [1i16, 2i16, 3i16]; // TODO read from IMU

        for i in 1..3 {
            self.angular_velocity[i] =
                dummy_angular_velocity[i] as f32 - self.angular_velocity_offset[i];
            // self.angular_velocity[0] is not used
        }

        self.cube_angle[0] += self.angular_velocity[2] as f32 * self.period / 1000f32 / 65.536f32;
        self.cube_angle[1] += self.angular_velocity[1] as f32 * self.period / 1000f32 / 65.536f32;

        // TODO use constant for rad -> degrees convertion
        let acceleration_angle = [
            libm::atan2f(dummy_acceleration[1] as f32, -dummy_acceleration[0] as f32) * 57.2958,
            libm::atan2f(dummy_acceleration[2] as f32, -dummy_acceleration[0] as f32) * 57.2958,
        ];
        self.fusion_measurements(acceleration_angle);

        self.cube_angle
    }

    fn get_filtered_angular_velocity(&mut self) -> [f32; 3] {
        // TODO use constatn for conversion to deg/s
        let filtered_angular_velocity = [
            0f32,
            self.filter[1].step(self.angular_velocity[1] / 131.0f32),
            self.filter[2].step(self.angular_velocity[2] / 131.0f32),
        ];
        filtered_angular_velocity
    }
}
