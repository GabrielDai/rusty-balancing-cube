// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

pub trait Filter {
    fn init(&mut self, value: f32);
    fn step(&mut self, new_value: f32) -> f32;
}

pub struct LowPassFilter {
    last_value: f32,
    constant: f32,
}

impl LowPassFilter {
    pub fn create(constant: f32) -> Self {
        LowPassFilter {
            last_value: 0f32,
            constant: constant,
        }
    }
}

impl Filter for LowPassFilter {
    fn init(&mut self, value: f32) {
        self.last_value = value;
    }

    fn step(&mut self, new_value: f32) -> f32 {
        self.last_value = self.constant * new_value + (1f32 - self.constant) * self.last_value;
        self.last_value
    }
}

pub struct LinearQuadraticRegulator {
    last_value: f32,
    gain: [f32; 3],
}

impl LinearQuadraticRegulator {
    pub fn create(gain: [f32; 3]) -> Self {
        LinearQuadraticRegulator {
            last_value: 0f32,
            gain: gain,
        }
    }

    pub fn step(&mut self, new_value: [f32; 2]) -> f32 {
        let value = self.gain[0] * new_value[0]
            + self.gain[1] * new_value[1]
            + self.gain[2] * self.last_value;
        value
    }

    pub fn sum_output(&mut self, output: f32) {
        self.last_value += output;
    }

    pub fn reset(&mut self) {
        self.last_value = 0f32;
    }
}
