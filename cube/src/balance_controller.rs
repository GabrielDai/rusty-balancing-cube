// SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

mod core;

use crate::actuators;
use crate::sensors;
use crate::setpoint;
use ::core::convert::From;
use ::core::convert::Into;

// State machine (see https://hoverbear.org/blog/rust-state-machine-pattern/)
pub struct StateMachine<S, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    core: core::Core<M, A, B>,
    state: S,
}

// States
pub struct Init;
pub struct Config;
pub struct DetectingBalancingPoint;
pub struct Balancing;

// State machine creation
impl<M, A, B> StateMachine<Init, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    pub fn create() -> Self {
        StateMachine {
            core: core::Core::create(),
            state: Init {},
        }
    }
}

// State machine transitions
// Init -> Config
impl<M, A, B> From<StateMachine<Init, M, A, B>> for StateMachine<Config, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    fn from(mut val: StateMachine<Init, M, A, B>) -> StateMachine<Config, M, A, B> {
        val.core.init();
        StateMachine {
            core: val.core,
            state: Config {},
        }
    }
}

// Config -> DetectingBalancingPoint
impl<M, A, B> From<StateMachine<Config, M, A, B>> for StateMachine<DetectingBalancingPoint, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    fn from(
        mut val: StateMachine<Config, M, A, B>,
    ) -> StateMachine<DetectingBalancingPoint, M, A, B> {
        val.configure();
        StateMachine {
            core: val.core,
            state: DetectingBalancingPoint {},
        }
    }
}

// DetectingBalancingPoint -> Config
impl<M, A, B> From<StateMachine<DetectingBalancingPoint, M, A, B>> for StateMachine<Config, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    fn from(
        mut val: StateMachine<DetectingBalancingPoint, M, A, B>,
    ) -> StateMachine<Config, M, A, B> {
        val.core.reset_offsets();
        StateMachine {
            core: val.core,
            state: Config {},
        }
    }
}

// DetectingBalancingPoint -> Balancing
impl<M, A, B> From<StateMachine<DetectingBalancingPoint, M, A, B>>
    for StateMachine<Balancing, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    fn from(
        mut val: StateMachine<DetectingBalancingPoint, M, A, B>,
    ) -> StateMachine<Balancing, M, A, B> {
        val.detect_balancing_point();
        StateMachine {
            core: val.core,
            state: Balancing {},
        }
    }
}

// Balancing -> DetectingBalancingPoint
impl<M, A, B> From<StateMachine<Balancing, M, A, B>>
    for StateMachine<DetectingBalancingPoint, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    fn from(
        mut val: StateMachine<Balancing, M, A, B>,
    ) -> StateMachine<DetectingBalancingPoint, M, A, B> {
        val.run_control_algorithm();
        val.core.standby();
        StateMachine {
            core: val.core,
            state: DetectingBalancingPoint {},
        }
    }
}

// State's methods
impl<M, A, B> StateMachine<Config, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    pub fn configure(&mut self) {
        loop {
            self.core.config();
            // TODO break when offsets have been updated
            break;
        }
    }
}

impl<M, A, B> StateMachine<DetectingBalancingPoint, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    pub fn detect_balancing_point(&mut self) {
        loop {
            // TODO routine for detecting balancing point
            break;
        }
    }
}

impl<M, A, B> StateMachine<Balancing, M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    pub fn run_control_algorithm(&mut self) {
        loop {
            self.core.control_balance();
            // TODO delay
            // TODO break if balancing point not recognized
            break;
        }
    }
}

pub enum BalanceController<M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    Init(StateMachine<Init, M, A, B>),
    Config(StateMachine<Config, M, A, B>),
    DetectingBalancingPoint(StateMachine<DetectingBalancingPoint, M, A, B>),
    Balancing(StateMachine<Balancing, M, A, B>),
}

impl<M, A, B> BalanceController<M, A, B>
where
    M: actuators::Motor,
    A: sensors::AttitudeMonitor,
    B: setpoint::BalancingPointSelector,
{
    pub fn execute_state(self) -> Self {
        match self {
            BalanceController::Init(val) => BalanceController::Config(val.into()),
            BalanceController::Config(val) => {
                BalanceController::DetectingBalancingPoint(val.into())
            }
            BalanceController::DetectingBalancingPoint(val) => {
                BalanceController::Balancing(val.into())
            }
            BalanceController::Balancing(val) => {
                BalanceController::DetectingBalancingPoint(val.into())
            }
        }
    }
}
