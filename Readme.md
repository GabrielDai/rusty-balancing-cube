<!--
SPDX-FileCopyrightText: 2024 Gabriel Moyano <vgmoyano@gmail.com>

SPDX-License-Identifier: MPL-2.0
-->

# Balancing-cube

**NOTE**: Unfortunately this project is not going to continue because of the lack of information about the motors (it is not possible to find a datasheet).
The motors used in the [original project](https://github.com/remrc/Self-Balancing-Cube) are Nidec 24H but keep in mind that you can find 2 serial numbers: 24H404H160 and 24H055M020.
If you still convinced to buy some of these motors, please DON'T BUY the one with the serial number 24H055M020 because the control signal is not a PWM signal, you have to control it changing the frequency of the signal.


Rusty implementation of [Self-Balancing-Cube](https://github.com/remrc/Self-Balancing-Cube).
The microcontroller that is going to be used is [xiao-rp2040](https://www.seeedstudio.com/XIAO-RP2040-v1-0-p-5026.html).
Nevertheless, the balancing logic is hardware-independent.
